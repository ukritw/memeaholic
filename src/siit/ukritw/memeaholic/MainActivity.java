package siit.ukritw.memeaholic;
import java.io.*;
import java.net.*;
import java.util.*;
import org.json.*;

import th.ac.tu.siit.lab8webservice.R;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

public class MainActivity extends ListActivity {
	//List<Map<String,String>> list;
	//SimpleAdapter adapter;


	ExtendedSimpleAdapter adapter;
	List<HashMap<String, Object>> list;
	Long lastUpdate = 0l;
	
	String nextPageUrl = null;
	Boolean morePage = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		list = new ArrayList<HashMap<String, Object>>();
		adapter = new ExtendedSimpleAdapter(this, list, R.layout.item, 
				new String[] {"caption", "meme"}, 
				new int[] {R.id.caption, R.id.meme});
		setListAdapter(adapter);


	}

	@Override
	protected void onStart() {
		super.onStart();
		//Check if the device has Internet connection
		ConnectivityManager mgr = (ConnectivityManager)
				getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			//Load data when there is a connection
			long current = System.currentTimeMillis();
			if (current - lastUpdate > 10*1000) {
				MemeTask task = new MemeTask(this);
				task.execute("http://infinigag.eu01.aws.af.cm/trending/0");
			}
		}
		else {
			Toast t = Toast.makeText(this, 
					"No Internet Connectivity on this device. Check your setting", 
					Toast.LENGTH_LONG);
			t.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId(); 
		switch(id) {
		case R.id.action_refresh:
			MemeTask task = new MemeTask(this);
			task.execute("http://infinigag.eu01.aws.af.cm/trending/");
			return true;
		case R.id.action_creatememe:
			Intent i = new Intent(this, MemeGenerator.class);
			startActivity(i);
			return true;
		case R.id.action_more:
			morePage = true;
			MemeTask taskMore = new MemeTask(this);
			taskMore.execute("http://infinigag.eu01.aws.af.cm/trending/"+nextPageUrl);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		} 
	}

	protected void onListItemClick(ListView l, View v, int position, long id) {
		String url = list.get(position).get("url").toString();
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(intent);

	}

	class MemeTask extends AsyncTask<String, Void, String> {
		//Map<String,String> record;
		HashMap<String, Object> record;
		ProgressDialog dialog;

		public MemeTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}

		//Executed in UI thread not Background thread (time-consuming task)
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading trending posts");
			dialog.show();
		}

		//Executed in UI thread
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
//			if (dialog.isShowing()) {
//				dialog.dismiss();
//			}
//			Toast t = Toast.makeText(getApplicationContext(), 
//					result, Toast.LENGTH_LONG);
//			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			morePage = false;
		}

		//Executed in Background thread 
		//Accept array of params
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				//Get the first parameter, set it as a URL
				URL url = new URL(params[0]);
				//Create a connection to the URL
				HttpURLConnection http = (HttpURLConnection)url.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				//We want to download data from the URL
				http.setDoInput(true);
				http.connect();

				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(http.getInputStream()));
					while((line = in.readLine()) != null) {
						buffer.append(line);
					}
					
					// Check if refresh or load more
					// Clear existing record in list
					if (!morePage)
						list.clear();
					JSONObject json = new JSONObject(buffer.toString());

					JSONArray jdata = json.getJSONArray("data");
					for(int i=0; i<jdata.length(); i++) {
						JSONObject dataObj = jdata.getJSONObject(i);
						String cap = dataObj.getString("caption");
						record = new HashMap<String,Object>();
						record.put("caption", cap);

						JSONObject imagesObj = dataObj.getJSONObject("images");
						String imgLink = imagesObj.getString("normal");

						Bitmap img;
						//create instance of InputStream and pass URL
						InputStream memeIn = new java.net.URL(imgLink).openStream();
						//decode stream and initialize bitmap 
						img = BitmapFactory.decodeStream(memeIn);

						record.put("meme", img);

						String linkToSite = dataObj.getString("link");
						record.put("url", linkToSite);

						list.add(record);

					}
					
					JSONObject pagingObj = json.getJSONObject("paging");
					nextPageUrl = pagingObj.getString("next");

					return "Finished Loading";
				}
				else {
					return "Error "+response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}	
		}
	}


}

